
-- Identite
INSERT INTO Identite(physique, pseudonyme_unique, roles)
VALUES
-- physique    pseudo       roles
  ( True,     'Alain',      B'000'),
  ( True,     'Bernard',    B'001'),
  ( True,     'Caroline',   B'010'),
  ( True,     'Damien',     B'011'),
  ( True,     'Églantine',  B'100'),
  ( True,     'Éric',       B'101'),
  ( True,     'Thör',       B'110'),
  ( True,     'Fañch',      B'111'),
  ( False,    'Imladris',   B'000'),
  ( False,    'Ærario',     B'000');


\echo '==== Identite ===='
SELECT * from Identite;

-- OEuvre
INSERT INTO OEuvre(nom)
VALUES -- nom
  ('Rendre le monde plus propre'),
  ('faire des calins et des bisous'),
  ('Sauver la forêt');

\echo '==== OEuvre ===='
SELECT * from OEuvre;



-- Equipe
INSERT INTO Equipe(nom, type, active, place_libre, gouvernance, indic_bien_etre)
VALUES
--   nom         type          active places gouvernance                 indicateur bien être
  ('Balayeurs', 'oeuvre',      true,  10,   'une gouvernance propre',   'Tout le monde va bien'),
  ('Toudoux',   'coopérative', true,  8,    'une gouvernance molle',    'Tout le monde dort'),
  ('Forêt',     'autre',       false, 0,    'les arbres ont le pouvoir','quantité de sève');

\echo '==== Equipe ===='
SELECT * from Equipe;


-- Lien Identite / Equipe

-- L'insertion de lien doit se faire à partir des UUID générés existantes dans les tables Identite et Equipe.
-- L'idée est de faire toutes les combinaisons possibles entre les deux tables (jointure CROSS JOIN),
-- et de ne sélectionner qu'une partie de ces liens.
-- Le type de lien est défini au hasard, en générant un nombre entre 1 et 7 converti en binaire
INSERT INTO Equipe_Identite(equipe_id,identite_id,type)
  SELECT e.id, i.id, (random()*7+1)::int::bit(4)  -- génère un entier entre 1 et 7 et le converti en bit
  FROM Equipe as e CROSS JOIN Identite as i        -- effectue toutes les combinaisons possibles des deux tables
  WHERE random() < 0.6;                            -- ne sélectionne que 60% des combinaisons

\echo '==== Equipe_Identite ===='

SELECT e.nom, e.id as id_équipe, i.pseudonyme_unique, i.physique, i.id as id_identité, lien.type
FROM Equipe as e
    JOIN Equipe_Identite as lien ON e.id=lien.equipe_id
    JOIN Identite as i ON i.id=lien.identite_id
ORDER BY e.nom, i.physique, i.pseudonyme_unique;


-- Lien Equipe / OEuvre

INSERT INTO Equipe_OEuvre(equipe_id, oeuvre_id, definition)
VALUES
    -- id_équipe                                      id_oeuvre                                                         definition
  ( (SELECT id FROM Equipe WHERE nom='Balayeurs'), (SELECT id FROM OEuvre WHERE nom='Rendre le monde plus propre'),     'Nous considérons que rendre le monde plus propre, c’est le rendre moins sale'),
  ( (SELECT id FROM Equipe WHERE nom='Toudoux'),   (SELECT id FROM OEuvre WHERE nom='faire des calins et des bisous'),  'C’est de l’affection partout et pour tout le monde. Surtout les gens méchant, qui sont particulièrement en manque de calin.'),
  ( (SELECT id FROM Equipe WHERE nom='Forêt'),     (SELECT id FROM OEuvre WHERE nom='Sauver la forêt'),                 'Sans les arbres, nous ne pourrions pas vivre heureux.');

\echo '==== Equipe / OEuvre ===='

SELECT e.nom, eo.equipe_id, o.nom, eo.oeuvre_id, eo.definition
FROM Equipe_OEuvre as eo
    JOIN Equipe as e ON eo.equipe_id=e.id
    JOIN OEuvre as o ON eo.oeuvre_id=o.id
ORDER BY e.nom, o.nom;


-- Ouvrage

INSERT INTO Ouvrage(equipe_id, oeuvre_id, presentation, date_debut, date_fin)
VALUES
  ( (SELECT id FROM Equipe WHERE nom='Balayeurs'), (SELECT id FROM OEuvre WHERE nom='Rendre le monde plus propre'), 'nettoyer la ville', '2022-01-01', '2022-05-20'),
  ( (SELECT id FROM Equipe WHERE nom='Forêt'),     (SELECT id FROM OEuvre WHERE nom='Sauver la forêt'),             'planter une forêt', '2020-07-15', '2021-12-11'),
  ( (SELECT id FROM Equipe WHERE nom='Forêt'),     (SELECT id FROM OEuvre WHERE nom='Sauver la forêt'),             'faire des chemins', '2022-02-18', '2022-03-19');

\echo '==== Ouvrage ===='

SELECT e.nom, o.equipe_id, eo.nom, o.oeuvre_id, o.presentation, o.date_debut, o.date_fin
FROM Ouvrage as o
  JOIN Equipe as e ON o.equipe_id=e.id
  JOIN OEuvre as eo ON o.oeuvre_id=eo.id
ORDER BY e.nom, o.date_debut;


\echo '==== Question ===='

INSERT INTO Question(demandeurid_id, question, solution)
  SELECT id,                                                               -- id du demandeur
         substring( md5(random()::text) from 0 for (random()*25+10)::int), -- crée une chaine de caractères au hasard (entre 10 et 35) pour le texte de la question
         random()>.5                                                       -- solution : True ou False
  FROM Identite
  WHERE random() < 0.5;  -- ne sélectionne que 50% des id

-- Répète 3 autres fois la même opération, afin d'avoir certaines id avec plusieurs questions dans leur formulaire

INSERT INTO Question(demandeurid_id, question, solution)
  SELECT id,
         substring( md5(random()::text) from 0 for (random()*25+10)::int),
         random()>.5
  FROM Identite
  WHERE random() < 0.5;

INSERT INTO Question(demandeurid_id, question, solution)
  SELECT id,
         substring( md5(random()::text) from 0 for (random()*25+10)::int),
         random()>.5
  FROM Identite
  WHERE random() < 0.5;

INSERT INTO Question(demandeurid_id, question, solution)
  SELECT id,
         substring( md5(random()::text) from 0 for (random()*25+10)::int),
         random()>.5
  FROM Identite
  WHERE random() < 0.5;
------------------------------------------------------------------------------



CREATE TABLE IF NOT EXISTS Reponse(
  repondant_id   UUID NOT NULL,
  question_id    UUID NOT NULL,
  reponse        boolean,

  PRIMARY KEY (repondant_id, question_id),
  CONSTRAINT fk_reponse_question_id FOREIGN KEY (question_id) REFERENCES Question(id),
  CONSTRAINT fk_reponse_repondant_id FOREIGN KEY (repondant_id) REFERENCES Identite(id)
);
