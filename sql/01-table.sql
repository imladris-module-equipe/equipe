--------------------------------------------------------------------------------
--                             Types Enum et Flag
--------------------------------------------------------------------------------

-- Roles (flag)

CREATE TYPE role AS ENUM (
  'cooperateur',
  'superviseur actionnaire',
  'actionnaire industrieux'
  );
CREATE DOMAIN roles bit(3) NOT NULL;

CREATE TABLE IF NOT EXISTS Flag_roles(
  flag roles,
  nom role,

  PRIMARY KEY (flag)
);
INSERT INTO Flag_roles VALUES (B'001', 'cooperateur');
INSERT INTO Flag_roles VALUES (B'010', 'superviseur actionnaire');
INSERT INTO Flag_roles VALUES (B'100', 'actionnaire industrieux');


-- Type d'inscription (flag)

CREATE TYPE type_inscription AS ENUM (
  'communauté',
  'postulant',
  'membre',
  'gouverne'
);
CREATE DOMAIN types_inscriptions bit(4) NOT NULL CHECK ( VALUE != B'0000');

CREATE TABLE IF NOT EXISTS Flag_type_inscription(
  flag types_inscriptions,
  nom type_inscription,

  PRIMARY KEY (flag)
);
INSERT INTO Flag_type_inscription VALUES (B'0001', 'communauté');
INSERT INTO Flag_type_inscription VALUES (B'0010', 'postulant');
INSERT INTO Flag_type_inscription VALUES (B'0100', 'membre');
INSERT INTO Flag_type_inscription VALUES (B'1000', 'gouverne');


-- Types d'équipe (enum)

CREATE TYPE type_equipe AS ENUM(
  'oeuvre',
  'coopérative',
  'autre'
);


--------------------------------------------------------------------------------
--                               Tables
--------------------------------------------------------------------------------


CREATE TABLE IF NOT EXISTS Identite (
  id                UUID         PRIMARY KEY DEFAULT gen_random_uuid (),
  physique          boolean      NOT NULL,
  pseudonyme_unique VARCHAR(255) UNIQUE NOT NULL,
  roles             roles
);

CREATE TABLE IF NOT EXISTS OEuvre (
  id                UUID         PRIMARY KEY DEFAULT gen_random_uuid (),
  nom               VARCHAR(255) UNIQUE NOT NULL
);


CREATE TABLE IF NOT EXISTS Equipe(
  id              UUID         PRIMARY KEY DEFAULT gen_random_uuid (),
  nom             varchar(255) NOT NULL,
  type            type_equipe  NOT NULL,
  active          boolean      NOT NULL DEFAULT true,
  place_libre     int          CHECK( 0 <= place_libre AND place_libre <= 12 ),
  gouvernance     text,
  indic_bien_etre text
);

CREATE TABLE IF NOT EXISTS Equipe_Identite(
  equipe_id         UUID               NOT NULL,
  identite_id       UUID               NOT NULL,
  type              types_inscriptions NOT NULL,
  date_inscription  date,
  date_postule      date,
  date_fin          date,

  PRIMARY KEY (equipe_id, identite_id),

  CONSTRAINT fk_Identite_membre_equipe_equipe_id FOREIGN KEY (equipe_id) REFERENCES Equipe(id),
  CONSTRAINT fk_Identite_membre_equipe_identite_id FOREIGN KEY (identite_id) REFERENCES Identite(id)
);

CREATE TABLE IF NOT EXISTS Equipe_OEuvre(
  equipe_id         UUID      NOT NULL,
  oeuvre_id         UUID      NOT NULL,
  definition        text      NOT NULL,

  PRIMARY KEY (equipe_id, oeuvre_id),

  CONSTRAINT fk_Equipe_OEuvre_equipe_id FOREIGN KEY (equipe_id) REFERENCES Equipe(id),
  CONSTRAINT fk_Equipe_OEuvre_oeuvre_id FOREIGN KEY (oeuvre_id) REFERENCES OEuvre(id)
);


CREATE TABLE IF NOT EXISTS Ouvrage(
  id              UUID    PRIMARY KEY DEFAULT gen_random_uuid(),
  equipe_id       UUID      NOT NULL,
  oeuvre_id       UUID      NOT NULL,
  presentation    text      NOT NULL,
  date_debut      date      NOT NULL ,
  date_fin        date      NOT NULL,

  CONSTRAINT fk_Ouvrage_equipe_id FOREIGN KEY (equipe_id) REFERENCES Equipe(id),
  CONSTRAINT fk_Ouvrage_oeuvre_id FOREIGN KEY (equipe_id, oeuvre_id) REFERENCES Equipe_OEuvre(equipe_id, oeuvre_id)
);


------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Question(
  id              UUID    PRIMARY KEY DEFAULT gen_random_uuid(),
  demandeurid_id  UUID    NOT NULL,
  question         text    NOT NULL,
  solution         boolean NOT NULL,

  CONSTRAINT fk_question_demandeurid_id FOREIGN KEY (demandeurid_id) REFERENCES Identite(id)
);


CREATE TABLE IF NOT EXISTS Reponse(
  repondant_id   UUID NOT NULL,
  question_id    UUID NOT NULL,
  reponse boolean,

  PRIMARY KEY (repondant_id, question_id),
  CONSTRAINT fk_reponse_question_id FOREIGN KEY (question_id) REFERENCES Question(id),
  CONSTRAINT fk_reponse_repondant_id FOREIGN KEY (repondant_id) REFERENCES Identite(id)
);
