#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant
from . import connection_BDD

from .base_oeuvre import BaseOEuvre
from .base_ouvrage import BaseOuvrage
from .base_presentation_equipe import BasePresentationEquipe


@dataclass(slots=True, frozen= True)
class PresentationEquipe(BasePresentationEquipe):
	""" cette classe permet d'y accedé à lensemble des informations sur une'équipe
 	et ses Ouvrage réalisées."""

	oeuvres: set[BaseOEuvre]
	ouvrages: set[BaseOuvrage]
