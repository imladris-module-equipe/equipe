#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .equipe import Equipe
from .oeuvre import OEuvre
from .base_definition import BaseDefinition

from . import connection_BDD

@dataclass(slots=True, frozen= True)
class Definition(BaseDefinition):
	""" cette classe permet de définir une oeuvre par une équipe"""
	equipe: Equipe
	oeuvre: OEuvre
