#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from . import connection_BDD
from .type_equipe import TypeEquipe



@dataclass(slots=True, frozen= True)
class BaseEquipe:
	""" c'est un ensemble de personne, compose de 12 membres au maximum,
	 et qui dispose une autonomie sur leur mission vis a vis
	 de l'assemblée générale  """

	identifiant: hash
	nom: str
	type_equipe: TypeEquipe
	active : bool
	place_libre : bool
	gouvernance: str
	indic_bien_etre: str

#	def __post_init__(self ):
#		if self.place_libre >0 and  


	@classmethod
	def get_all(cls) :
		"""Récupère toutes les instances de BaseEquipe"""
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id,nom,type,active,place_libre,gouvernance,indic_bien_etre FROM Equipe;")
			les_equipes=set[BaseEquipe]()

			for une_ligne in cur:
				uuid,nom,le_type,active,place_libre,gouvernance, indic_bien_etre = une_ligne

				le_type = TypeEquipe(le_type)

				base_equipe = BaseEquipe(
					identifiant     = uuid,
					nom             = nom,
					type_equipe     = le_type,
					active          = active,
					place_libre     = place_libre,
					gouvernance     = gouvernance,
					indic_bien_etre = indic_bien_etre
				)
				les_equipes.add(base_equipe)
			# fin de boucle
		# fin de with
		return les_equipes


	@classmethod
	def get_all_ids(cls) :
		"""Récupère tous les identifiants de la table Equipe"""
		#creation de set d'identifiants d'equipes (de la classe string)
		les_equipes=set[str]()
		#conncetion a la BDD et choisir une colone id de la table Equipe
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id from Equipe")
			# je parcours l'ensemble des identifiant d'objets de ma classe equipe
			for une_ligne in cur:
				#j'ajoute chaque identifiant d'objet à mon set d'identifiants equipes
				uuid = une_ligne[0]
				les_equipes.add(uuid)
		# je renvoie le set
		return les_equipes


	@classmethod
	def get_one(cls, id:str) :
		"""Récupère une instance de la classe Equipe à partir de son identifiant"""
		# Récupération des données de l'instance
			# connection à la base
		with connection_BDD.cursor() as cur :
			# récupération de la ligne
			cur.execute("SELECT id, nom, type, active, place_libre, gouvernance, indic_bien_etre FROM Equipe where id=(%s)", (id,))

			une_ligne_equipe=cur.fetchone()

		uuid,nom,type_equipe,active,place_libre,gouvernance,indic_bien_etre = une_ligne_equipe

		# formatage en python
		type_equipe=TypeEquipe(type_equipe)
		active=bool(active)
		place_libre=int(place_libre)
		
		# créer une instance de BaseEquipe
		base_equipe = BaseEquipe(
			identifiant     = uuid,
			nom             = nom,
			type_equipe     = type_equipe,
			active          = active,
			place_libre     = place_libre,
			gouvernance     = gouvernance,
			indic_bien_etre = indic_bien_etre
		)
		# return l'instance de BaseEquipe
		return base_equipe
