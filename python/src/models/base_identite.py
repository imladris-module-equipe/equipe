#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .role import Role
from . import connection_BDD


@dataclass(slots=True, frozen= True)
class BaseIdentite:
	"""cette classe est un masque social, qui sert à masque
	l'identité de la personne, dans leur interaction
	avec les groupes de personnes"""

	identifiant: hash
	roles: Role
	physique: bool
	pseudonyme_unique: str

	@classmethod
	def get_all(cls) :
		"""Récupère toutes les instances de BaseIdentite"""

		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id, physique, pseudonyme_unique, roles from Identite")
			les_identites=set[BaseIdentite]()

			for une_ligne in cur:
				uuid, physique, pseudo, roles = une_ligne

				roles = Role(int(roles,2))

				base_identite = BaseIdentite(
					identifiant=uuid,
					roles=roles,
					physique=physique,
					pseudonyme_unique=pseudo
					)

				les_identites.add(base_identite)
			# fin de boucle
		# fin de with
		return les_identites

	@classmethod
	def get_all_ids(cls) :
		"""Récupère tous les identifiants de la table Identite"""
		les_identites=set[str]()
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id from Identite")
			for une_ligne in cur:
				uuid = une_ligne[0]
				les_identites.add(uuid)
		return les_identites


	@classmethod
	def get_one(cls, id:str) :
		"""Récupère une instance de la classe BaseIdentite à partir de son identifiant"""
		with connection_BDD.cursor() as cur :

			cur.execute("SELECT id, physique, pseudonyme_unique, roles from Identite where id=(%s)", (id,))
			une_ligne=cur.fetchone()
		uuid, physique, pseudo,roles = une_ligne
		roles=Role(int(roles,2))
		base_identite = BaseIdentite(
			identifiant=uuid,
			roles=roles,
			physique=physique,
			pseudonyme_unique=pseudo
			)

		return base_identite
