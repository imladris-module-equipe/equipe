#!/usr/bin/env python3
# -*-coding:utf-8 -*

from enum import Flag, unique
from . import connection_BDD

@unique
class TypeInscription(Flag):
	""" cette classe permet d'identifier dans qu'elle role peut-on inscrire
une personne au sein d'une équipe."""

	communaute = 0b0001
	postulant  = 0b0010
	membre     = 0b0100
	gouverne   = 0b1000
