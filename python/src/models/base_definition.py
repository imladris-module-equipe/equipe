#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant


from . import connection_BDD

@dataclass(slots=True, frozen= True)
class BaseDefinition:
	""" cette classe permet de faire la définition d'une oeuvre pour une équipe"""
	identifiant_equipe: hash
	identifiant_oeuvre: hash
	definition: str

	@classmethod
	def get_all_oeuvres_ids(cls, id_equipe):
		"""
		Récupère tous les identifiants de toutes les oeuvres dans lesquelles s'est inscrite une équipe
		id_equipe : l'identifiant de l'équipe
		return : set des identifiants des oeuvres
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT oeuvre_id from Equipe_OEuvre where equipe_id=(%s)", (id_equipe,) )

			les_identifiants=set[str]()
			for une_ligne in cur:
				oeuvre_id=une_ligne[0]
				les_identifiants.add(oeuvre_id)

		return les_identifiants

	@classmethod
	def get_all_equipes_ids(cls, id_oeuvre):
		"""
		Récupère tous les identifiants des équipes inscrites à une oeuvre
		id_oeuvre : l'identifiant de l'oeuvre
		return : set des identifiants de ses équipes
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT equipe_id from Equipe_OEuvre where oeuvre_id=(%s)", (id_oeuvre,) )

			les_identifiants=set[str]()
			for une_ligne in cur:
				equipe_id=une_ligne[0]
				les_identifiants.add(equipe_id)

		return les_identifiants




	@classmethod
	def get_one(cls, id_equipe, id_oeuvre):
		"""
		récupère l'instance d'une BaseDefinition entre une Equipe et une OEuvre
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT equipe_id, oeuvre_id, definition from Equipe_OEuvre where equipe_id=(%s) and oeuvre_id=(%s) ", (id_equipe, id_oeuvre) )
			#il est censé avoir qu'une seul reponse(une ligne)
			une_ligne=cur.fetchone()

		uuid_equipe, uuid_oeuvre, definition = une_ligne

		base_definition=BaseDefinition(
			identifiant_equipe	= uuid_equipe,
			identifiant_oeuvre	= uuid_oeuvre,
			definition			= definition
			)
		return base_definition
		







