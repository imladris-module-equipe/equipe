#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .base_identite import BaseIdentite
from . import connection_BDD


@dataclass(slots=True, frozen= True)
class Identite(BaseIdentite):
	"""cette classe est un masque social, qui sert à masque 
	l'identité de la personne, dans leur interaction 
	avec les groupes de personnes"""

	@staticmethod
	def moinsdedouzepersone(mavariable:set):
		pass 

