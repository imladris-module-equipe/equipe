#!/usr/bin/python3.10
#-*- coding: utf-8 -*-

import sys
from pathlib import Path
import psycopg2
from environnement import arguments

# Ajoute le répertoire models comme base de recherche de module
root = Path(__file__).parents[1].resolve()
if not str(root) in sys.path :
    sys.path.append(str(root))


connection_BDD = psycopg2.connect(
	dbname	= arguments.pg_database,
	user	= arguments.pg_user,
	password= arguments.pg_password,
	host	= arguments.pg_host
)
