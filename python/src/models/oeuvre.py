#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .base_oeuvre import BaseOEuvre
from . import connection_BDD


@dataclass(slots=True, frozen= True)
class OEuvre(BaseOEuvre):
	"""Cette classe est une OEuvre"""
	pass
