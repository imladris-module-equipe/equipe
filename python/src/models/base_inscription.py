#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant
from datetime import date

from .type_inscription import TypeInscription
from . import connection_BDD

@dataclass(slots=True, frozen= True)
class BaseInscription:
	""" cette classe permet de faire l'adhesion d'un cooperateur dans une équipe 
"""
	identifiant_identite: hash
	identifiant_equipe: hash
	date_inscription: date
	date_fin: date
	date_postule: date
	type_inscription: TypeInscription

	@classmethod
	def get_all_identites_ids(cls, id_equipe):
		"""
		il récupère tous les identifiants des identites inscrites à une équipe
		id_equipe : l'identifiant de l'équipe
		return : set des identifiants des identités inscrites à cette équipe
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT identite_id from Equipe_Identite where equipe_id=(%s)", (id_equipe,) )
			
			les_identifiant=set[str]()
			for une_ligne in cur:
				identite_id=une_ligne[0]
				les_identifiant.add(identite_id)
				
		return les_identifiant
	
	@classmethod
	def get_all_equipes_ids(cls, id_identite):
		"""
		il récupère tous les identifiants des équipes auxquelles une identité est inscrite
		id_identite : l'identifiant de l'identité
		return : set des identifiants de ses équipes
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT equipe_id from Equipe_Identite where identite_id=(%s)", (id_identite,) )
			
			les_identifiant=set[str]()
			for une_ligne in cur:
				equipe_id=une_ligne[0]
				les_identifiant.add(equipe_id)
				
		return les_identifiant




	@classmethod
	def get_one(cls, id_equipe, id_identite):
		"""
		récupère l'instance d'une BaseInscription entre une Identite et une Equipe
		"""
		# connection à la base
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT equipe_id,identite_id, type, date_inscription, date_postule, date_fin from Equipe_Identite where equipe_id=(%s) and identite_id=(%s) ", (id_equipe, id_identite) )
			
			une_ligne_inscription=cur.fetchone()
			
		uuid_equipe,uuid_identite,type_inscription, date_inscription, date_postule, date_fin=une_ligne_inscription
		
		type_inscription=TypeInscription(int(type_inscription, 2))
		
		base_inscription=BaseInscription(
			identifiant_identite= uuid_identite,
			identifiant_equipe  = uuid_equipe,
			date_inscription    = date_inscription,
			date_fin            = date_fin,
			date_postule        = date_postule,
			type_inscription    = type_inscription
			)
		return base_inscription










