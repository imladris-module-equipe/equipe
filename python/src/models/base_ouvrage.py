#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2

from datetime import date
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant
from . import connection_BDD



@dataclass(slots=True, frozen= True)
class BaseOuvrage():
	""" cette classe est une réalisation d'une équipe """
	identifiant: hash
	presentation : str
	date_debut: date
	date_fin: date


	@classmethod
	def get_all(cls) :
		"""Récupère toutes les instances de BaseOuvrage"""

		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id, equipe_id, oeuvre_id, presentation, date_debut, date_fin FROM Ouvrage;")
			les_ouvrages=set[BaseOuvrage]()

			for une_ligne in cur:
				uuid, equipe_id, oeuvre_id, presentation, date_debut, date_fin = une_ligne
				# date_debut et date_fin sont déjà formaté en objet date par psycopg

				base_ouvrage = BaseOuvrage(
					identifiant= uuid,
					presentation = presentation,
					date_debut= date_debut,
					date_fin= date_fin
					)
				les_ouvrages.add(base_ouvrage)
			# fin de boucle
		# fin de with
		return les_ouvrages


	@classmethod
	def get_all_ids(cls) :
		"""Récupère tous les identifiants de la table Ouvrage"""

		les_ouvrages=set[str]()
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id from Ouvrage;")
			for une_ligne in cur:
				uuid = une_ligne[0]
				les_ouvrages.add(uuid)
		return les_ouvrages

	@classmethod
	def get_one(cls, id:str) :
		"""Récupère une instance de la classe BaseOuvrage à partir de son identifiant"""
		with connection_BDD.cursor() as cur :

			cur.execute("SELECT id, equipe_id, oeuvre_id, presentation, date_debut, date_fin FROM Ouvrage where id=(%s);", (id,))

			une_ligne=cur.fetchone()

		uuid, equipe_id, oeuvre_id, presentation, date_debut, date_fin= une_ligne

		# date_debut et date_fin sont déjà formaté en objet date par psycopg

		base_ouvrage = BaseOuvrage(
			identifiant = uuid,
			presentation = presentation,
			date_debut = date_debut,
			date_fin = date_fin
			)

		return base_ouvrage
