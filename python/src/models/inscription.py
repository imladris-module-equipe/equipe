#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .equipe import Equipe
from .identite import Identite
from .base_inscription import BaseInscription

from . import connection_BDD

@dataclass(slots=True, frozen= True)
class Inscription(BaseInscription):
	""" cette classe permet de faire l'adhesion d'un cooperateur dans une équipe
"""
	membre: Identite
	equipe: Equipe
