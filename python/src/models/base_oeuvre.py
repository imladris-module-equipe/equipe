#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from . import connection_BDD


@dataclass(slots=True, frozen= True)
class BaseOEuvre:
	""" c'est un ensemble de personne, compose de 12 membres au maximum,
	 et qui dispose une autonomie sur leur mission vis a vis
	 de l'assemblée générale  """

	identifiant: hash
	nom: str

	@classmethod
	def get_all(cls) :
		"""Récupère toutes les instances de BaseOEuvre"""
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id,nom FROM OEuvre;")
			les_oeuvres=set[BaseOEuvre]()

			for une_ligne in cur:
				uuid,nom = une_ligne

				base_oeuvre = BaseOEuvre(
					identifiant     = uuid,
					nom             = nom
				)
				les_oeuvres.add(base_oeuvre)
			# fin de boucle
		# fin de with
		return les_oeuvres


	@classmethod
	def get_all_ids(cls) :
		"""Récupère tous les identifiants de la table OEuvre"""
		#creation de set d'identifiants d'oeuvres (de la classe string)
		les_oeuvres=set[str]()
		#conncetion a la BDD et choisir une colone id de la table OEuvre
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id from OEuvre")
			# je parcours l'ensemble des identifiant d'objets de ma classe oeuvre
			for une_ligne in cur:
				#j'ajoute chaque identifiant d'objet à mon set d'identifiants oeuvres
				uuid = une_ligne[0]
				les_oeuvres.add(uuid)
		# je renvoie le set
		return les_oeuvres


	@classmethod
	def get_one(cls, id:str) :
		"""Récupère une instance de la classe OEuvre à partir de son identifiant"""
		# Récupération des données de l'instance
			# connection à la base
		with connection_BDD.cursor() as cur :
			# récupération de la ligne
			cur.execute("SELECT id, nom FROM OEuvre where id=(%s)", (id,))

			une_ligne_oeuvre=cur.fetchone()

		uuid, nom = une_ligne_oeuvre

		# formatage en python
		# créer une instance de BaseOEuvre
		base_oeuvre = BaseOEuvre(
			identifiant     = uuid,
			nom             = nom
		)

		# return l'instance de BaseOEuvre
		return base_oeuvre
