#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant
from . import connection_BDD


@dataclass(slots=True, frozen= True)
class BaseInteractionForum:
	""" cette classe permet d'établir la communication entre les membres d'une 
équipe et les personnes souhaitant s'informé sur une équipe dans le but 
d'adheré une équipe"""
	
	serveur_oauth : str


