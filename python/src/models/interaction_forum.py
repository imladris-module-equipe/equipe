#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .equipe import Equipe
from .identite import Identite
from  .base_interaction_forum import BaseInteractionForum

from . import connection_BDD

@dataclass(slots=True, frozen= True)
class InteractionForum(BaseInteractionForum):
	""" cette classe permet d'établir la communication entre les membres d'une 
équipe et les personnes souhaitant s'informé sur une équipe dans le but 
d'adheré une équipe"""

	equipe : Equipe
	identite: Identite

	@staticmethod
	def oauth(mavariable:Identite):
		pass
