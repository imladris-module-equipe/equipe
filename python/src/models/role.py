#!/usr/bin/env python3
# -*-coding:utf-8 -*

from enum import Flag

class Role(Flag):
	""" cette classe identifie les droit d'acces des identités
	au sein de la cooperative """

	cooperateurs            = 0b001
	superviseur_actionnaire = 0b010
	actionnaire_industrieux = 0b100
