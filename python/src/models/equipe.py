#!/usr/bin/env python3
# -*-coding:utf-8 -*

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .base_equipe import BaseEquipe
from .base_identite import BaseIdentite
from .base_oeuvre import BaseOEuvre
from .base_ouvrage import BaseOuvrage
from .base_presentation_equipe import BasePresentationEquipe
from . import connection_BDD



@dataclass(slots=True, frozen= True)
class Equipe(BaseEquipe):
	""" c'est un ensemble de personne, compose de 12 membres au maximum,
	et qui dispose une autonomie sur leur mission vis a vis
	de l'assemblée générale  """

	oeuvres: set[BaseOEuvre]
	ouvrages: set[BaseOuvrage]
	membres: set[BaseIdentite]
	communaute: set[BaseIdentite]
	liste_gouvernance :set[BaseIdentite]


	@staticmethod
	def get_all_from_identite(identite : BaseIdentite):
		pass

	@staticmethod
	def get_all_from_membre(membre : BaseIdentite):
		pass

	@staticmethod
	def get_all_from_communaute(communaute : BaseIdentite):
		pass

	@staticmethod
	def get_all_from_gouvernant(gouverne : BaseIdentite):
		pass

	@property
	def presentation(self) -> BasePresentationEquipe :
		pass
