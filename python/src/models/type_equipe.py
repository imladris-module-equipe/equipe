#!/usr/bin/env python3
# -*-coding:utf-8 -*

from enum import Enum
from . import connection_BDD

class TypeEquipe(Enum):
	""" cette classe permet de savoir la nature de l'équipe, afin d'identifier son
	niveau d'autonomie vis à vis de l'assemblée générale"""
	
	# les valeurs sont celles de l'enum dans la base SQL
	equipe_oeuvre = 'oeuvre'
	equipe_coop   = 'coopérative'
	autre         = 'autre'
