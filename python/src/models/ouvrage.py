#!/usr/bin/env python3
# -*-coding:utf-8 -*

from datetime import date
from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant
from . import connection_BDD
from .base_ouvrage import BaseOuvrage
from .base_equipe import BaseEquipe
from .base_oeuvre import BaseOEuvre

from . import connection_BDD

@dataclass(slots=True, frozen= True)
class Ouvrage(BaseOuvrage):
	""" cette classe est une réalisation d'une équipe """

	oeuvre: BaseOEuvre
	equipe: BaseEquipe

	@staticmethod
	def ouvrageDeLBaseEquipe( equipe : BaseEquipe ) -> set[BaseOuvrage] :
		pass
