#!/usr/bin/env python3
# -*-coding:utf-8 -*
import psycopg2

from dataclasses import dataclass
from hashlib import sha1 # pour faire un hash de l'identifiant

from .type_equipe import TypeEquipe
from . import connection_BDD

@dataclass(slots=True, frozen= True)
class BasePresentationEquipe:
	""" cette classe permet d'acceder à l'ensemble des informations sur une équipe
 	et ses Ouvrages réalisées."""

	identifiant: hash
	nom : str
	active : bool
	gouvernance : str
	indic_bien_etre : str
	type_equipe : TypeEquipe


	@classmethod
	def get_all(cls) :
		"""Récupère toutes les instances de BasePresentationEquipe"""
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id,nom,type,active,gouvernance, indic_bien_etre FROM Equipe;")
			les_equipes=set[BasePresentationEquipe]()

			for une_ligne in cur:
				uuid,nom,le_type,active,gouvernance, indic_bien_etre = une_ligne

				le_type = TypeEquipe(le_type)

				base_equipe = BasePresentationEquipe(
					identifiant     = uuid,
					nom             = nom,
					type_equipe     = le_type,
					active          = active,
					gouvernance     = gouvernance,
					indic_bien_etre = indic_bien_etre
				)
				les_equipes.add(base_equipe)
			# fin de boucle
		# fin de with
		return les_equipes

	@classmethod
	def get_all_ids(cls) :
		"""Récupère tous les identifiants de la vue de présentation des équipes"""
		# connection a la BDD et choisir une colone id de la table Equipe
		with connection_BDD.cursor() as cur :
			cur.execute("SELECT id from Equipe")

			#creation de set d'identifiants d'equipes (de la classe string)
			les_equipes=set[str]()

			# je parcours l'ensemble des identifiant d'objets de ma classe equipe
			for une_ligne in cur:
				uuid = une_ligne[0]
				#j'ajoute chaque identifiant d'objet à mon set d'identifiants equipes
				les_equipes.add(uuid)
		# je renvoie le set
		return les_equipes


	@classmethod
	def get_one(cls, id:str) :
		"""Récupère une instance de la classe BasePresentationEquipe à partir de son identifiant"""
		# Récupération des données de l'instance
			# connection à la base
		with connection_BDD.cursor() as cur :
			# récupération de la ligne
			cur.execute("SELECT id, nom, type, active, gouvernance, indic_bien_etre FROM Equipe where id=(%s)", (id,))
			une_ligne_equipe=cur.fetchone()

		uuid,nom,type_equipe,active,gouvernance,indic_bien_etre = une_ligne_equipe

		# formatage en python
		type_equipe=TypeEquipe(type_equipe)

		# créer une instance de BaseEquipe
		base_equipe = BasePresentationEquipe(
			identifiant     = uuid,
			nom             = nom,
			type_equipe     = type_equipe,
			active          = active,
			gouvernance     = gouvernance,
			indic_bien_etre = indic_bien_etre
		)
		# return l'instance de BaseEquipe
		return base_equipe
