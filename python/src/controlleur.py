#! /bin/python3
#-*- coding: utf-8 -*-

from . import app
from flask import render_template, request, redirect, url_for, abort
from models.base_equipe import BaseEquipe
from werkzeug.exceptions import HTTPException, default_exceptions

class PaymentRequired(HTTPException):
	""" Ajout d'une classe d'erreur 402, non apportée par défaut en flask """
	code = 402
	description = '<p>Payment required.</p>'


@app.route("/index.html", methods=["GET"])
@app.route("/", methods=["GET"])
def landing_page():
	""" affiche la landing_page"""

	return render_template("landing_page.html")


@app.route("/accueil", methods=["GET"])
def accueil():
	""" affiche la accueil"""

	return render_template("accueil.html")


@app.route("/contact", methods=["GET"])
def contact():
	""" affiche les contacts"""

	return render_template("contact.html")


@app.route("/aider-moi", methods=["GET"])
def aider_moi():
	""" affiche la page trouver une équipe qui peux m'aider'"""

	return render_template("aider_moi.html")


@app.route("/comptabilite-finance", methods=["GET"])
def comptabilite_finance():
	""" Affiche la comptabilité par équipe pour une supervision des actionnaires"""

	return render_template("compta_et_finance.html")


@app.route("/mes-equipes", methods=["GET"])
def mes_equipes():
	""" Affiche la page équipe et partenaires
	C'est la page où retrouver ses équipes de travail, placer ses tickets de caisse ou échanger avec les autres équipes avec lesquelles ont travail
	"""

	return render_template("equipes_et_partenaires.html")


@app.route("/ouvrages", methods=["GET"])
def ouvrages():
	""" affiche la page des actualités et de présentations des ouvrages réalisés."""

	return render_template("nouvelles_et_ouvrages.html")


@app.route("/participer", methods=["GET"])
def participer():
	""" affiche la page pour s'inscrire à des équipes afin d'aider les autres ou une oeuvre qui nous intéresse."""

	return render_template("participer.html")



@app.route("/participer-resultats", methods=["GET"])
def participer_resultats():
	""" affiche la page de résultat"""

	# On utilise la méthode "get" du dictionnaire "args", pour éviter de faire planter l'application au cas où le paramètre "oeuvres" n'aurai pas été transmis.
	oeuvres = request.args.get('oeuvres','')

	# Récupération en base des équipes.
	# FIXME faire une récupération des seules équipes concernées par les oeuvres, pas toutes celles de la base.
	ma_liste = BaseEquipe.get_all()

	return render_template("participer_resultat.html", ma_liste=ma_liste, oeuvres=oeuvres)



@app.route("/participer/<id_equipe>", methods=["GET"])
def participer_une_equipe(id_equipe):
	""" affiche la page d'une équipe'"""
	une_equipe=BaseEquipe.get_one(id_equipe)
	return render_template("participer_une_equipe.html",une_equipe=une_equipe)



@app.route("/profil", methods=["GET"])
def profil():
	""" affiche la page de profil"""

	return render_template("profil.html")


@app.route("/deconnexion", methods=["GET"])
def deconnexion():
	""" affiche la page de profil"""

	return redirect(url_for('accueil'))

@app.route("/connexion", methods=["GET"])
def connexion():
	""" affiche la page de profil"""
	raise PaymentRequired()

	return render_template("profil.html")


# Définition des traitements en cas d'exceptions

@app.errorhandler(404)
def page_not_found(error):
	return render_template('404.html'), 404

@app.errorhandler(PaymentRequired)
def page_non_developpee(error):
	print(error)
	return render_template('402.html'), 402
