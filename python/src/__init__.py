#! /bin/python3
#-*- coding: utf-8 -*-

import sys
from pathlib import Path
from flask import Flask

# Ajoute le répertoire src comme base de recherche de module
root = Path(__file__).parent.resolve()
if not str(root) in sys.path :
	sys.path.append(str(root))

app = Flask(__name__)

