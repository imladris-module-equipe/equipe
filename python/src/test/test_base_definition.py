#!/usr/bin/env python3
# -*-coding:utf-8 -*

from datetime import date
from unittest import TestCase

from models.base_definition import BaseDefinition
from models.definition		import Definition
from models.base_equipe		import BaseEquipe
from models.base_oeuvre		import BaseOEuvre

from test.random_text 		import RandomText
import test.instanciation as instanciation


class TestBaseDefinition(TestCase):

	liste_definitions = (
		'Nous considérons que rendre le monde plus propre, c’est le rendre moins sale',
		'C’est de l’affection partout et pour tout le monde. Surtout les gens méchant, qui sont particulièrement en manque de calin.',
		'Sans les arbres, nous ne pourrions pas vivre heureux.'
	);

	def test_init(self):
		"""teste l'instanciation de la classe BaseDefinition"""
		definition = RandomText.description()
		base_definition=BaseDefinition(
			identifiant_equipe	= 0,
			identifiant_oeuvre	= 1,
			definition			= definition
			)

		self.assertIsInstance(base_definition, BaseDefinition)
		self.assertEqual(base_definition.identifiant_equipe, 0)
		self.assertEqual(base_definition.identifiant_oeuvre, 1)
		self.assertEqual(base_definition.definition, definition)

	def test_get_all_oeuvres_ids(self):
		""" Teste la méthode BaseDefinition.get_all_oeuvres_ids() """

		les_identifiants_oeuvres = BaseOEuvre.get_all_ids()
		for id_equipe in BaseEquipe.get_all_ids():
			liste = BaseDefinition.get_all_oeuvres_ids(id_equipe)

			self.assertIsInstance(liste, set)
			self.assertTrue( les_identifiants_oeuvres.issuperset(liste) )
			for une_id in liste:
				self.assertIsInstance(une_id, str)
				self.assertTrue( len(une_id)>0 )

	def test_get_all_equipes_ids(self):
		""" Teste la méthode BaseDefinition.test_get_all_equipes_ids() """
		les_identifiant_equipes = BaseEquipe.get_all_ids()
		for id_oeuvre in BaseOEuvre.get_all_ids():
			liste = BaseDefinition.get_all_equipes_ids(id_oeuvre)

			self.assertIsInstance(liste, set)
			self.assertTrue( les_identifiant_equipes.issuperset(liste) )
			for une_id in liste:
				self.assertIsInstance(une_id, str)
				self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		"""Test la méthode BaseDefinition.get_one(id_equipe, id_oeuvre)"""

		toutes_les_equipes = BaseEquipe.get_all_ids()
		toutes_les_oeuvres = BaseOEuvre.get_all_ids()

		for id_equipe in toutes_les_equipes:
			for id_oeuvre in BaseDefinition.get_all_oeuvres_ids(id_equipe):

				definition = BaseDefinition.get_one(id_equipe, id_oeuvre)

				self.assertIsInstance( definition, BaseDefinition )
				self.assertIn( definition.identifiant_equipe, toutes_les_equipes )
				self.assertIn( definition.identifiant_oeuvre, toutes_les_oeuvres )
				self.assertIsInstance( definition.definition, str )

class TestDefinition(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe Definition"""

		une_equipe = instanciation.instancier_equipe()
		une_oeuvre = instanciation.instancier_oeuvre()
		une_definition = RandomText.description()

		definition=Definition(

			# propriétés héritées de BaseDefinition
			identifiant_equipe = 0,
			identifiant_oeuvre = 1,
			definition= une_definition,

			# propriétés non hérités de Definition
			equipe= une_equipe,
			oeuvre= une_oeuvre
		)

		# test des propiétés hérités de BaseDefinition
		self.assertIsInstance(definition, BaseDefinition)
		self.assertEqual(definition.identifiant_equipe, 0)
		self.assertEqual(definition.identifiant_oeuvre, 1)
		self.assertEqual(definition.definition, une_definition)

		# test des propiétés non hérités de Definition
		self.assertIsInstance(definition, Definition)
		self.assertEqual(definition.equipe, une_equipe)
		self.assertEqual(definition.oeuvre, une_oeuvre)
