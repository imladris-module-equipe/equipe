#!/usr/bin/env python3
# -*-coding:utf-8 -*

import random
from datetime import date
from time import time

class RandomText:
	
	base_prenom = ('Alain','Bernard','Caroline','Damien','Églantine', 'Éric', 'Thör','Fañch')
	base_nom 	= ('Dupont','Crouton','Bouchon','Charpentier','LeLièvre','Näzgül','Œuf','Glloq', 'Bønjour')
	random_boolean=(True, False)
	
	base_valeur = ('Abondance', 'Acceptation', 'Accessibilité', 'Accomplissement', 'Achèvement', 'Actualité', 'Adaptabilité', 'Adoration', 'Affection', 'Affluence', 'Agilité', 'Agressivité', 'Altruisme', 'Ambition', 'Anticipation', 'Appréciation', 'Assertivité', 'Assurance', 'Astucieux', 'Attentisme', 'Attraction', 'Audace', 'Auto-discipline', 'Autonomie', 'Aventure', 'Beauté', 'Bienveillance', 'Bonheur', 'Bravoure', 'Calme', 'Camaraderie', 'Candeur', 'Caritatif', 'Célébrité', 'Certitude', 'Chaleur', 'Charme', 'Chasteté', 'Clarté', 'Cohérence', 'Compassion', 'Compétence', 'Compréhension', 'Concentration', 'Confiance en soi', 'Conformité', 'Confort', 'Connexion', 'Conscience', 'Contentement', 'Continuité', 'Contribution', 'Contrôle', 'Conviction', 'Convivialité', 'Coopération', 'Cordialité', 'Courage', 'Courtoisie', 'Créativité', 'Crédibilité', 'Croissance', 'Curiosité', 'Découverte', 'Déférence', 'Défi', 'Délectation', 'Désir', 'Détermination', 'Devoir', 'Dévotion', 'Dextérité', 'Dignité', 'Diligence', 'Discernement', 'Discipline', 'Discrétion', 'Disponibilité', 'Diversité', 'Domination', 'Don', 'Dynamisme', 'Économe', 'Éducation', 'Efficacité', 'Égalité', 'Élégance', 'Empathie', 'Empressement', 'Encouragement', 'Endurance', 'Énergie', 'Engagement', 'Entêtement', 'Enthousiasme', 'Équité', 'Espérance', 'Être le meilleur', 'Euphorie', 'Exactitude', 'Excellence', 'Expérience', 'Expertise', 'Exploration', 'Expressivité', 'Extase', 'Extravagance', 'Exubérance', 'Famille', 'Fascination', 'Fermeté', 'Férocité', 'Fiabilité', 'Fidélité', 'Finesse', 'Flexibilité', 'Force', 'Fraîcheur', 'Franchise', 'Frugalité', 'Fun', 'Furtif', 'Gagner', 'Gaieté', 'Galanterie', 'Générosité', 'Grâce', 'Gratitude', 'Habileté', 'Harmonie intérieure', 'Héroïsme', 'Honnêteté', 'Immobilisme', 'Importance', 'Intelligence', 'Intrépidité', 'Jeunesse', 'Joie', 'Liberté', 'L’indépendance financière', 'Lucidité', 'Maîtrise', 'Obligation', 'Ordre social', 'Orientation', 'Paix', 'Partage', 'Performance', 'Plaisir', 'Politesse', 'Pouvoir social', 'Prise de décision', 'Profondeur', 'Propreté', 'Proximité', 'Raffinement', 'Réciprocité', 'Réflexion', 'Relaxation', 'Repos', 'Résilience', 'Respect', 'Retenue', 'Rêve', 'Révérence', 'Richesse', 'Rigueur', 'Sacré', 'Sagesse', 'Sang-froid', 'Santé', 'Satisfaction', 'Sécurité', 'Self-contrôle', 'Sens de la vie', 'Sensibilité', 'Sentiment d’appartenance', 'Sérénité', 'Service', 'Sexualité', 'Silence', 'Simplicité', 'Sincérité', 'Solidarité', 'Solidité', 'Solitude', 'Soutien', 'Spiritualité', 'Spontanéité', 'Stabilité', 'Succès', 'Suprématie', 'Surprise', 'Sympathie', 'Synergie', 'Tradition', 'Tranquilité', 'Transcendance', 'Travail d’équipe', 'Unité', 'Utilité', 'Valeur', 'Variété', 'Vérité', 'Vie spirituelle', 'Vigilance', 'Vision', 'Vitalité', 'Vitesse', 'Vivacité', 'Volonté')

	@staticmethod
	def prenom_nom():
		return random.choice(RandomText.base_prenom) + ' ' + random.choice(RandomText.base_nom)

	@staticmethod
	def prenom():
		return random.choice(RandomText.base_prenom)

	@staticmethod
	def nom():
		return random.choice(RandomText.base_nom)

	@staticmethod
	def random_date(nb:int=1, mini:date=date.fromtimestamp(1), maxi:date=date.today()):
		""" Crée une ou des dates aléatoires entre deux dates. Le résultat étant trié.
			Par défaut, renvoit une seule date entre le 01/01/1970 et aujourd'hui
			nb : nombre de date a générer (strictement positif)
			mini : date minimum (inclue), par défaut le 01/01/1970
			maxi : date maximum (exclue), par défaut aujourd'hui

			renvoit : une date si nb=1, une liste de date si nb est supérieur

			usages : 
			ma_date = random_date()
			debut, fin = random_date(2)
			debut, pendant, fin = random_date(3)
			liste_date = random_date(15)

		"""

		# Test les entrées
		assert nb>0, 'nb doit être positif'
		assert isinstance(mini,date), 'mini doit être une date'
		assert isinstance(maxi,date), 'maxi doit être une date'

		# convertit en timestamp mini et maxi
		mini = int(mini.strftime('%s'))
		maxi = int(maxi.strftime('%s'))

		# Crée la liste de date à partir de timestamp
		tab = [ random.randint(mini,maxi) for i in range(nb) ]
		tab.sort() # trie la liste
		tab = [ date.fromtimestamp( t ) for t in tab ]
		
		return tab[0] if nb==1 else tab # Si un seul élément dans la liste, renvoit la date plutôt que la liste.	


	@staticmethod
	def oeuvre():
		return random.choice(RandomText.base_valeur)

	@staticmethod
	def boolean():
		return random.choice(RandomText.random_boolean)
		
	@staticmethod
	def identifiant():
		return random.randint(0,1000)
		
	@staticmethod
	def description(min:int=10,max:int=200):
		nb_mots = random.randrange(int(min), int(max))
		tab = random.choices(RandomText.base_valeur, k=nb_mots)
		return " ".join(tab)
		
	
