#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase

from models.base_interaction_forum import BaseInteractionForum
from models.interaction_forum import  InteractionForum

from test.random_text import RandomText
import test.instanciation as instanciation



class TestBaseInteractionForum(TestCase):

	liste_serveur_oauth_intercationforums=()

	def test_init(self):
		"""teste l'instanciation de la classe BaseInteractionForum"""

		baseintercationforum=BaseInteractionForum(
			serveur_oauth= "ping"
			)
		self.assertIsInstance(baseintercationforum, BaseInteractionForum)
		self.assertEqual(baseintercationforum.serveur_oauth, "ping")


class TestInteractionForum(TestCase):
	def test_init(self):
		"""teste l'instanciation de la classe InteractionForum"""

		une_identite=instanciation.instancier_identite()
		une_equipe = instanciation.instancier_equipe()

		interactionforum=InteractionForum(
			# propriétés héritées de BaseInteractionForum
			serveur_oauth= "ping",

			# propriétés non hérités de Equipe
			equipe= une_equipe,
			identite= une_identite
			)


		# test des propiétés hérités de BaseInteractionForum
		self.assertIsInstance(interactionforum, BaseInteractionForum)
		self.assertEqual(interactionforum.serveur_oauth, "ping")

		# test des propiétés non hérités de InteractionForum
		self.assertIsInstance(interactionforum, InteractionForum)
		self.assertEqual(interactionforum.equipe, une_equipe)
		self.assertEqual(interactionforum.identite, une_identite)
