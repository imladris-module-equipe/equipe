#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase
import random
import test.instanciation as instanciation

from models.base_oeuvre import BaseOEuvre
from models.oeuvre import OEuvre

class TestBaseOEuvre(TestCase):

	liste_nom_oeuvre = ('Rendre le monde plus propre','faire des calins et des bisous','Sauver la forêt');

	def test_init(self):
		"""teste l'instanciation de la classe BaseOEuvre"""
		baseoeuvre=BaseOEuvre(
			identifiant=0,
			nom="toto"
		)

		self.assertIsInstance(baseoeuvre, BaseOEuvre)
		self.assertEqual(baseoeuvre.identifiant, 0)
		self.assertEqual(baseoeuvre.nom, "toto")

	def test_get_all(self):
		""" teste la méthode BaseOEuvre.get_all() """
		les_oeuvres=BaseOEuvre.get_all()
		nb = len(self.liste_nom_oeuvre)

		self.assertIsInstance(les_oeuvres, set)
		self.assertEqual(len(les_oeuvres), nb)

		les_noms = set[str]()
		for une_oeuvre in les_oeuvres:
			self.assertIsInstance(une_oeuvre, BaseOEuvre)
			self.assertTrue(une_oeuvre.nom in self.liste_nom_oeuvre)
			les_noms.add(une_oeuvre.nom)

		self.assertEqual(len(les_noms), nb)

	def test_get_all_ids(self):
		""" teste la méthode BaseOEuvre.get_all_ids() """
		a_tester = BaseOEuvre.get_all_ids()
		taille=len(a_tester)
		self.assertIsInstance(a_tester, set)
		self.assertEqual(taille,len(self.liste_nom_oeuvre))
		for une_id in a_tester:
			self.assertIsInstance(une_id, str)
			self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		""" teste la méthode BaseOEuvre.get_one(id) """

		nb = len(self.liste_nom_oeuvre)
		les_noms = set[str]()
		for id in BaseOEuvre.get_all_ids():
			une_oeuvre = BaseOEuvre.get_one(id)
			self.assertIsInstance(une_oeuvre, BaseOEuvre)
			self.assertTrue(une_oeuvre.nom in self.liste_nom_oeuvre)
			les_noms.add(une_oeuvre.nom)

		self.assertEqual(len(les_noms), nb)


class TestOEuvre(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe OEuvre"""

		oeuvre=OEuvre(
			# propriétés héritées de BaseOEuvre
			identifiant=0,
			nom="toto"

			# propriétés de OEuvre non hérités
			# aucune
			)

		# test des propiétés hérités de BaseOEuvre
		self.assertIsInstance(oeuvre, BaseOEuvre)
		self.assertEqual(oeuvre.identifiant, 0)
		self.assertEqual(oeuvre.nom, "toto")

		# test des propiétés de OEuvre non hérités
		self.assertIsInstance(oeuvre, OEuvre)
