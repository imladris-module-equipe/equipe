#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase
from models.base_identite import BaseIdentite
from models.role import Role
from models.identite import Identite
import test.instanciation as instanciation

class TestBaseIdentite(TestCase):

	liste_pseudonymes_identites=('Alain','Bernard','Caroline','Damien','Églantine','Éric','Thör','Fañch','Imladris','Ærario')

	def test_init(self):
		"""teste l'instanciation de la classe BaseIdentite"""
		baseidentite=BaseIdentite(
			identifiant=0,
			roles= Role.cooperateurs,
			physique=True,
			pseudonyme_unique="TI"
			)

		self.assertEqual(baseidentite.identifiant, 0)
		self.assertIsInstance(baseidentite, BaseIdentite)
		self.assertEqual(baseidentite.roles, Role.cooperateurs)
		self.assertEqual(baseidentite.pseudonyme_unique, "TI")
		self.assertTrue(BaseIdentite.physique)


	def test_get_all(self):
		""" teste la méthode BaseIdentite.get_all() """
		les_identites=BaseIdentite.get_all()
		nb = len(self.liste_pseudonymes_identites)
		self.assertIsInstance(les_identites, set)
		self.assertEqual(len(les_identites), nb)

		les_pseudonymes = set[str]()
		for une_identite in les_identites:
			self.assertIsInstance(une_identite, BaseIdentite)
			self.assertTrue(une_identite.pseudonyme_unique in self.liste_pseudonymes_identites)
			les_pseudonymes.add(une_identite.pseudonyme_unique)
		self.assertEqual(len(les_pseudonymes), nb)


	def test_get_all_ids(self):
		""" teste la méthode BaseIdentite.get_all_ids() """
		a_tester = BaseIdentite.get_all_ids()
		taille=len(a_tester)
		self.assertIsInstance(a_tester, set)
		self.assertEqual(taille,len(self.liste_pseudonymes_identites))
		for une_id in a_tester:
			self.assertIsInstance(une_id, str)
			self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		""" teste la méthode BaseIdentite.get_one(id) """

		nb = len(self.liste_pseudonymes_identites)
		les_pseudonymes = set[str]()

		for id in BaseIdentite.get_all_ids():
			une_identite = BaseIdentite.get_one(id)
			self.assertIsInstance(une_identite, BaseIdentite)
			self.assertTrue(une_identite.pseudonyme_unique in self.liste_pseudonymes_identites)
			les_pseudonymes.add(une_identite.pseudonyme_unique)

		self.assertEqual(len(les_pseudonymes), nb)


class TestIdentite(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe Identite"""
		identite= Identite(
			identifiant=0,
			roles= Role.cooperateurs,
			physique=True,
			pseudonyme_unique="TI"
			)

		self.assertIsInstance(identite, Identite)
		#teste de l'heritage
		self.assertIsInstance(identite, BaseIdentite)
		self.assertEqual(identite.identifiant, 0)
		self.assertEqual(identite.roles, Role.cooperateurs)
		self.assertEqual(identite.pseudonyme_unique, "TI")
		self.assertTrue(identite.physique)
