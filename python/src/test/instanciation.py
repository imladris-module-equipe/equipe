#!/usr/bin/env python3
# -*-coding:utf-8 -*

from models.type_equipe import TypeEquipe
from models.role import Role
from models.type_inscription import TypeInscription

from test.random_text import RandomText

from models.base_equipe import BaseEquipe
from models.base_identite import BaseIdentite
from models.base_oeuvre import BaseOEuvre
from models.base_ouvrage import BaseOuvrage
from models.base_presentation_equipe import BasePresentationEquipe
from models.base_inscription import BaseInscription
from models.base_interaction_forum import BaseInteractionForum

from models.equipe import Equipe
from models.identite import Identite
from models.oeuvre import OEuvre
from models.ouvrage import Ouvrage
from models.presentation_equipe import PresentationEquipe
from models.inscription import Inscription
from models.interaction_forum import InteractionForum


def instancier_base_equipe():

	base_equipe=BaseEquipe(
		identifiant=RandomText.identifiant(),
		nom=RandomText.nom(),
		type_equipe=TypeEquipe.equipe_oeuvre,
		active=RandomText.boolean(),
		place_libre=RandomText.boolean(),
		gouvernance= RandomText.description(),
		indic_bien_etre=  RandomText.description(),
		)
	return base_equipe


def instancier_base_oeuvre():
	base_oeuvre=BaseOEuvre(
		identifiant = RandomText.identifiant(),
		nom = RandomText.oeuvre()
		)
	return base_oeuvre


def instancier_oeuvre():
	oeuvre=OEuvre(
		identifiant = RandomText.identifiant(),
		nom = RandomText.oeuvre()
		)
	return oeuvre


def instancier_ouvrage():
	debut, fin = RandomText.random_date(2)

	ouvrage=Ouvrage(
		identifiant=RandomText.identifiant(),
		presentation = RandomText.description(),
		date_debut= debut,
		date_fin= fin,
		equipe=instancier_base_equipe(),
		oeuvre=instancier_base_oeuvre()
		)
	return ouvrage



def instancier_base_ouvrage():
	debut, fin = RandomText.random_date(2)

	# les ouvrages
	base_ouvrage=BaseOuvrage(
		identifiant=RandomText.identifiant(),
		presentation =  RandomText.description(),
		date_debut= debut,
		date_fin= fin,
		)
	return base_ouvrage


# les identitées
def instancier_base_identite():

	base_identite=BaseIdentite(
		identifiant=RandomText.identifiant(),
		roles= Role.cooperateurs,
		physique=RandomText.boolean(),
		pseudonyme_unique=RandomText.prenom_nom()
		)
	return base_identite





def instancier_equipe(nb_communaute=4,nb_membre=3,nb_gouv=2,nb_oeuvre=1,nb_ouvrages=2):

	la_communaute = set()
	les_membres = set()
	la_gouvernance = set()
	les_oeuvres = set()
	les_ouvrages = set()

	for i in range(nb_communaute):
		base_identite = instancier_base_identite()
		la_communaute.add(base_identite)

	for i in range(nb_membre):
		base_identite = instancier_base_identite()
		les_membres.add(base_identite)

	for i in range(nb_gouv):
		base_identite = instancier_base_identite()
		la_gouvernance.add(base_identite)

	for i in range(nb_oeuvre):
		base_oeuvre = instancier_base_oeuvre()
		les_oeuvres.add(base_oeuvre)

	for i in range(nb_ouvrages):
		base_ouvrage = instancier_base_ouvrage()
		les_ouvrages.add(base_ouvrage)


	equipe=Equipe(
		# propriétés héritées de BaseEquipe
		identifiant		= RandomText.identifiant(),
		nom				= RandomText.nom(),
		type_equipe 	= TypeEquipe.equipe_oeuvre,
		active			= RandomText.boolean(),
		place_libre		= RandomText.boolean(),
		gouvernance		= RandomText.description(),
		indic_bien_etre	= RandomText.description(),

		# propriétés non hérités de Equipe
		oeuvres			= les_oeuvres,
		ouvrages		= les_ouvrages,
		membres			= les_membres,
		communaute		= la_communaute,
		liste_gouvernance= la_gouvernance
		)

	return equipe

def instancier_identite():

	identite=Identite(
		identifiant=0,
		roles= Role.cooperateurs,
		physique=RandomText.description(),
		pseudonyme_unique=RandomText.prenom_nom()
		)
	return identite

def instancier_base_inscription():
	postule, debut, fin = RandomText.random_date(3)

	base_inscription=BaseInscription(
		date_Inscription= debut,
		date_fin= fin,
		date_postule= postule,
		type_inscription= TypeInscription.membre
		)
	return base_inscription

def instancier_inscription():

	postule, debut, fin = RandomText.random_date(3)

	inscription=Inscription(

		# propriétés héritées de BaseInscription
		date_Inscription= debut,
		date_fin= fin,
		date_postule= postule,
		type_inscription= TypeInscription.membre,


		# propriétés non hérités de Inscription
		membre= instancier_identite(),
		equipe= instancier_equipe()

		)
	return inscription



def instancier_base_interaction_forum():

	baseintercationforum=BaseInteractionForum(
		serveur_oauth= "ping"
		)
	return baseintercationforum

def instancier_interaction_forum():

	interactionforum=InteractionForum(
	# propriétés héritées de BaseInteractionForum
		serveur_oauth= "ping",

	# propriétés non hérités de Equipe
		equipe= instancier_equipe(),
		identite= instancier_identite()
		)
	return interactionforum


def instancier_base_presentation_equipe():

	base_presentation_equipe=BasePresentationEquipe(
		nom= RandomText.nom(),
		active= RandomText.boolean(),
		gouvernance= RandomText.description(),
		indic_bien_etre= RandomText.description(),
		type_equipe = TypeEquipe.equipe_oeuvre
		)
	return base_presentation_equipe


def instancier_presentation_equipe():

	presentation_equipe=PresentationEquipe(

		# propriétés héritées de BasePresentationEquipe
		nom= RandomText.nom(),
		active= RandomText.boolean(),
		gouvernance=RandomText.description(),
		indic_bien_etre= RandomText.description(),
		type_equipe = TypeEquipe.equipe_oeuvre,

		# propriétés non hérités de PresentationEquipe
		ouvrage= instancier_base_ouvrage()

	)
	return presentation_equipe
