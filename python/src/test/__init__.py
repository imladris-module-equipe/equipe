#!/usr/bin/python3.10
#-*- coding: utf-8 -*-

import sys
from pathlib import Path

# Ajoute le répertoire src comme base de recherche de module
root = Path(__file__).parents[1].resolve()
if not str(root) in sys.path :
    sys.path.append(str(root))

