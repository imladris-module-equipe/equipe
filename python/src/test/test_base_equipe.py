#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase
import random
import test.instanciation as instanciation

from models.base_equipe import BaseEquipe
from models.type_equipe import TypeEquipe
from models.equipe import Equipe

class TestBaseEquipe(TestCase):

	liste_nom_equipes = ('Balayeurs','Toudoux','Forêt')

	def test_init(self):
		"""teste l'instanciation de la classe BaseEquipe"""
		baseequipe=BaseEquipe(
			identifiant=0,
			nom="toto",
			type_equipe=TypeEquipe.equipe_oeuvre,
			active=True,
			place_libre=True,
			gouvernance= "gov",
			indic_bien_etre= "IDH"
		)

		self.assertIsInstance(baseequipe, BaseEquipe)
		self.assertEqual(baseequipe.identifiant, 0)
		self.assertEqual(baseequipe.nom, "toto")
		self.assertEqual(baseequipe.type_equipe, TypeEquipe.equipe_oeuvre)
		self.assertTrue(baseequipe.active)
		self.assertTrue(baseequipe.place_libre)
		self.assertEqual(baseequipe.gouvernance, "gov")
		self.assertEqual(baseequipe.indic_bien_etre, "IDH")

	def test_get_all(self):
		""" teste la méthode BaseEquipe.get_all() """
		les_equipes=BaseEquipe.get_all()
		nb = len(self.liste_nom_equipes)

		self.assertIsInstance(les_equipes, set)
		self.assertEqual(len(les_equipes), nb)

		les_noms = set[str]()
		for une_equipe in les_equipes:
			self.assertIsInstance(une_equipe, BaseEquipe)
			self.assertTrue(une_equipe.nom in self.liste_nom_equipes)
			les_noms.add(une_equipe.nom)

		self.assertEqual(len(les_noms), nb)

	def test_get_all_ids(self):
		""" teste la méthode BaseEquipe.get_all_ids() """
		a_tester = BaseEquipe.get_all_ids()
		taille=len(a_tester)
		self.assertIsInstance(a_tester, set)
		self.assertEqual(taille,len(self.liste_nom_equipes))
		for une_id in a_tester:
			self.assertIsInstance(une_id, str)
			self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		""" teste la méthode BaseEquipe.get_one(id) """

		nb = len(self.liste_nom_equipes)
		les_noms = set[str]()
		for id in BaseEquipe.get_all_ids():
			une_equipe = BaseEquipe.get_one(id)
			self.assertIsInstance(une_equipe, BaseEquipe)
			self.assertTrue(une_equipe.nom in self.liste_nom_equipes)
			les_noms.add(une_equipe.nom)

		self.assertEqual(len(les_noms), nb)


class TestEquipe(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe Equipe"""
		la_communaute = set()
		les_membres = set()
		la_gouvernance = set()
		les_oeuvres = set()
		les_ouvrages = set()

		for i in range(4):
			base_identite = instanciation.instancier_base_identite()
			la_communaute.add(base_identite)

		for i in range(3):
			base_identite = instanciation.instancier_base_identite()
			les_membres.add(base_identite)

		for i in range(2):
			base_identite = instanciation.instancier_base_identite()
			la_gouvernance.add(base_identite)

		for i in range(1):
			base_oeuvre = instanciation.instancier_base_oeuvre()
			les_oeuvres.add(base_oeuvre)

		for i in range(2):
			base_ouvrage = instanciation.instancier_base_ouvrage()
			les_ouvrages.add(base_ouvrage)

		equipe=Equipe(
			# propriétés héritées de BaseEquipe
			identifiant=0,
			nom="toto",
			type_equipe=TypeEquipe.equipe_oeuvre,
			active=True,
			place_libre=True,
			gouvernance= "gov",
			indic_bien_etre= "IDH",

			# propriétés non hérités de Equipe
			oeuvres= les_oeuvres,
			ouvrages= les_ouvrages,
			membres= les_membres,
			communaute= la_communaute,
			liste_gouvernance= la_gouvernance
			)

		# test des propiétés hérités de BaseEquipe
		self.assertIsInstance(equipe, BaseEquipe)
		self.assertEqual(equipe.identifiant, 0)
		self.assertEqual(equipe.nom, "toto")
		self.assertEqual(equipe.type_equipe, TypeEquipe.equipe_oeuvre)
		self.assertTrue(equipe.active)
		self.assertTrue(equipe.place_libre)
		self.assertEqual(equipe.gouvernance, "gov")
		self.assertEqual(equipe.indic_bien_etre, "IDH")

		# test des propiétés non hérités de Equipe
		self.assertIsInstance(equipe, Equipe)
		self.assertEqual(equipe.oeuvres, les_oeuvres)
		self.assertEqual(equipe.ouvrages, les_ouvrages)
		self.assertEqual(equipe.membres, les_membres)
		self.assertEqual(equipe.communaute, la_communaute)
		self.assertEqual(equipe.liste_gouvernance,la_gouvernance)




	def test_presentation(self):
		pass
