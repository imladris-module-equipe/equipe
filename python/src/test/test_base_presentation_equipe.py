#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase

from models.base_presentation_equipe import BasePresentationEquipe
from models.presentation_equipe import PresentationEquipe
from models.type_equipe import TypeEquipe

import test.instanciation as instanciation
from test.random_text import RandomText



class TestBasePresentationEquipe(TestCase):

	liste_nom_presentationequipes = ('Balayeurs','Toudoux','Forêt')


	def test_init(self):
		"""teste l'instanciation de la classe BasePresentationEquipe"""

		basepresentationequipe=BasePresentationEquipe(
			identifiant=0,
			nom= "toto",
			active= True,
			gouvernance= "gov",
			indic_bien_etre= "IDH",
			type_equipe = TypeEquipe.equipe_oeuvre
			)

		self.assertIsInstance(basepresentationequipe, BasePresentationEquipe)
		self.assertEqual(basepresentationequipe.identifiant, 0)
		self.assertEqual(basepresentationequipe.nom, "toto")
		self.assertTrue(basepresentationequipe.active)
		self.assertEqual(basepresentationequipe.gouvernance, "gov")
		self.assertEqual(basepresentationequipe.indic_bien_etre, "IDH" )
		self.assertEqual(basepresentationequipe.type_equipe,TypeEquipe.equipe_oeuvre)


	def test_get_all(self):
		""" teste la méthode BasePresentationEquipe.get_all """

		les_presentationequipes=BasePresentationEquipe.get_all()
		nb = len(self.liste_nom_presentationequipes)

		self.assertIsInstance(les_presentationequipes, set)
		self.assertEqual(len(les_presentationequipes), nb)

		les_noms = set[str]()
		for une_presentationequipe in les_presentationequipes:
			self.assertIsInstance(une_presentationequipe, BasePresentationEquipe)
			self.assertTrue(une_presentationequipe.nom in self.liste_nom_presentationequipes)
			les_noms.add(une_presentationequipe.nom)

		self.assertEqual(len(les_noms), nb)

	def test_get_all_ids(self):
		""" teste la méthode BasePresentationEquipe.get_all_ids """
		a_tester = BasePresentationEquipe.get_all_ids()
		taille=len(a_tester)
		self.assertIsInstance(a_tester, set)
		self.assertEqual(taille,len(self.liste_nom_presentationequipes))
		for une_id in a_tester:
			self.assertIsInstance(une_id, str)
			self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		""" teste la méthode BasePresentationEquipe.get_one """

		nb = len(self.liste_nom_presentationequipes)
		les_noms = set[str]()
		for id in BasePresentationEquipe.get_all_ids():
			une_presentationequipe = BasePresentationEquipe.get_one(id)
			self.assertIsInstance(une_presentationequipe, BasePresentationEquipe)
			self.assertTrue(une_presentationequipe.nom in self.liste_nom_presentationequipes)
			les_noms.add(une_presentationequipe.nom)

		self.assertEqual(len(les_noms), nb)



class TestPresentationEquipe(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe PresentationEquipe"""

		les_oeuvres = set()
		les_ouvrages = set()

		for i in range(1):
			base_oeuvre = instanciation.instancier_base_oeuvre()
			les_oeuvres.add(base_oeuvre)

		for i in range(2):
			base_ouvrage = instanciation.instancier_base_ouvrage()
			les_ouvrages.add(base_ouvrage)

		presentationequipe=PresentationEquipe(

			# propriétés héritées de BasePresentationEquipe
			identifiant=0,
			nom= "toto",
			active= True,
			gouvernance= "gov",
			indic_bien_etre= "IDH",
			type_equipe = TypeEquipe.equipe_oeuvre,

			# propriétés non hérités de PresentationEquipe
			oeuvres=les_oeuvres,
			ouvrages=les_ouvrages
			)

		# test des propiétés hérités de BasePresentationEquipe
		self.assertIsInstance(presentationequipe, BasePresentationEquipe)
		self.assertEqual(presentationequipe.identifiant, 0)
		self.assertEqual(presentationequipe.nom, "toto")
		self.assertTrue(presentationequipe.active)
		self.assertEqual(presentationequipe.gouvernance, "gov")
		self.assertEqual(presentationequipe.indic_bien_etre, "IDH" )
		self.assertEqual(presentationequipe.type_equipe, TypeEquipe.equipe_oeuvre)

		# test des propiétés non hérités de PresentationEquipe
		self.assertIsInstance(presentationequipe, PresentationEquipe)
		self.assertEqual(presentationequipe.oeuvres, les_oeuvres)
		self.assertEqual(presentationequipe.ouvrages, les_ouvrages)
