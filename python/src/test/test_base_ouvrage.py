#!/usr/bin/env python3
# -*-coding:utf-8 -*

from unittest import TestCase
from datetime import date

from models.base_ouvrage import BaseOuvrage
from models.base_oeuvre import BaseOEuvre
from models.ouvrage import Ouvrage

from test.random_text import RandomText
import test.instanciation as instanciation

class TestBaseOuvrage(TestCase):

	liste_presentation_ouvrages = ('nettoyer la ville','planter une forêt','faire des chemins')

	def test_init(self):
		"""teste l'instanciation de la classe BaseOuvrage"""

		debut, fin = RandomText.random_date(2)
		baseouvrage=BaseOuvrage(
			identifiant =0,
			presentation = "a",
			date_debut= debut,
			date_fin= fin
			)

		self.assertIsInstance(baseouvrage, BaseOuvrage)
		self.assertEqual(baseouvrage.identifiant,0)
		self.assertEqual(baseouvrage.presentation, "a")
		self.assertEqual(baseouvrage.date_debut, debut)
		self.assertTrue(baseouvrage.date_fin, fin)

	def test_get_all(self):
		""" teste la méthode BaseOuvrage.get_all() """

		les_ouvrages=BaseOuvrage.get_all()
		nb = len(self.liste_presentation_ouvrages)

		self.assertIsInstance(les_ouvrages, set)
		self.assertEqual(len(les_ouvrages), nb)

		les_presentations = set[str]()
		for un_ouvrage in les_ouvrages:
			self.assertIsInstance(un_ouvrage, BaseOuvrage)
			self.assertTrue(un_ouvrage.presentation in self.liste_presentation_ouvrages)
			les_presentations.add(un_ouvrage.presentation)

		self.assertEqual(len(les_presentations), nb)

	def test_get_all_ids(self):
		""" teste la méthode BaseOuvrage.get_all_ids() """
		a_tester = BaseOuvrage.get_all_ids()
		taille=len(a_tester)
		self.assertIsInstance(a_tester, set)
		self.assertEqual(taille,len(self.liste_presentation_ouvrages))
		for une_id in a_tester:
			self.assertIsInstance(une_id, str)
			self.assertTrue( len(une_id)>0 )

	def test_get_one(self):
		""" teste la méthode BaseOuvrage.get_one(id) """

		nb = len(self.liste_presentation_ouvrages)
		les_presentations = set[str]()
		for id in BaseOuvrage.get_all_ids():
			un_ouvrage = BaseOuvrage.get_one(id)
			self.assertIsInstance(un_ouvrage, BaseOuvrage)
			self.assertTrue(un_ouvrage.presentation in self.liste_presentation_ouvrages)
			les_presentations.add(un_ouvrage.presentation)

		self.assertEqual(len(les_presentations), nb)




class TestOuvrage(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe Ouvrage"""

		debut, fin = RandomText.random_date(2)

		baseequipe=instanciation.instancier_base_equipe()
		baseoeuvre=instanciation.instancier_base_oeuvre()

		ouvrage=Ouvrage(
			identifiant = 0,
			presentation = "a",
			date_debut= debut,
			date_fin= fin,

			equipe=baseequipe,
			oeuvre=baseoeuvre
		)

		self.assertIsInstance(ouvrage, BaseOuvrage)
		self.assertEqual(ouvrage.identifiant,0)
		self.assertEqual(ouvrage.presentation, "a")
		self.assertEqual(ouvrage.date_debut, debut)
		self.assertTrue(ouvrage.date_fin, fin)


		self.assertIsInstance(ouvrage, Ouvrage)
		self.assertEqual(ouvrage.equipe, baseequipe)
		self.assertEqual(ouvrage.oeuvre, baseoeuvre)
