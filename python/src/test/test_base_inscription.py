#!/usr/bin/env python3
# -*-coding:utf-8 -*

from datetime import date
from unittest import TestCase

from models.base_inscription import BaseInscription
from models.type_inscription import TypeInscription
from models.inscription      import Inscription
from models.base_equipe      import BaseEquipe
from models.base_identite    import BaseIdentite

from test.random_text import RandomText
import test.instanciation as instanciation


class TestBaseInscription(TestCase):

	liste_type_inscriptions = ()

	def test_init(self):
		"""teste l'instanciation de la classe BaseInscription"""

		postule, debut, fin = RandomText.random_date(3)

		baseinscription=BaseInscription(
			identifiant_identite = 0,
			identifiant_equipe   = 1,
			date_inscription     = debut,
			date_fin             = fin,
			date_postule         = postule,
			type_inscription     = TypeInscription.membre
			)

		self.assertIsInstance(baseinscription, BaseInscription)
		self.assertEqual(baseinscription.identifiant_identite, 0)
		self.assertEqual(baseinscription.identifiant_equipe, 1)
		self.assertEqual(baseinscription.date_inscription, debut)
		self.assertEqual(baseinscription.date_fin, fin)
		self.assertEqual(baseinscription.date_postule, postule)
		self.assertTrue(baseinscription.type_inscription, TypeInscription.membre )
	
	def test_get_all_identites_ids(self):
		""" Teste la méthode BaseInscription.get_all_identites_ids() """
		les_identifiant_identites = BaseIdentite.get_all_ids()
		for id_equipe in BaseEquipe.get_all_ids():
			liste = BaseInscription.get_all_identites_ids(id_equipe)
			
			self.assertIsInstance(liste, set)
			self.assertTrue( les_identifiant_identites.issuperset(liste) )
			for une_id in liste:
				self.assertIsInstance(une_id, str)
				self.assertTrue( len(une_id)>0 )
			
	def test_get_all_equipes_ids(self):
		""" Teste la méthode BaseInscription.test_get_all_equipes_ids() """
		les_identifiant_equipes = BaseEquipe.get_all_ids()
		for id_identite in BaseIdentite.get_all_ids():
			liste = BaseInscription.get_all_equipes_ids(id_identite)
			
			self.assertIsInstance(liste, set)
			self.assertTrue( les_identifiant_equipes.issuperset(liste) )
			for une_id in liste:
				self.assertIsInstance(une_id, str)
				self.assertTrue( len(une_id)>0 )
				
	def test_get_one(self):
		"""Test la méthode BaseInscription.get_one(id_equipe, id_identite)"""
		
		toutes_les_equipes   = BaseEquipe.get_all_ids()
		toutes_les_identites = BaseIdentite.get_all_ids()
		
		for id_equipe in toutes_les_equipes:
			for id_identite in BaseInscription.get_all_identites_ids(id_equipe):
				
				inscription = BaseInscription.get_one(id_equipe, id_identite)
				
				self.assertIsInstance( inscription, BaseInscription )
				self.assertIn( inscription.identifiant_equipe, toutes_les_equipes )
				self.assertIn( inscription.identifiant_identite, toutes_les_identites )
				self.assertIsInstance( inscription.type_inscription, TypeInscription )
				
				if inscription.date_inscription != None :
					self.assertIsInstance(inscription.date_inscription, date)
				if inscription.date_fin != None :
					self.assertIsInstance(inscription.date_fin, date)
				if inscription.date_postule != None :
					self.assertIsInstance(inscription.date_postule, date)
				
				
class TestInscription(TestCase):

	def test_init(self):
		"""teste l'instanciation de la classe Inscription"""

		une_identite=instanciation.instancier_identite()
		une_equipe = instanciation.instancier_equipe()

		postule, debut, fin = RandomText.random_date(3)

		inscription=Inscription(
			
			# propriétés héritées de BaseInscription
			identifiant_identite = 0,
			identifiant_equipe   = 1,
			date_inscription= debut,
			date_fin= fin,
			date_postule= postule,
			type_inscription= TypeInscription.membre,


			# propriétés non hérités de Inscription
			membre= une_identite,
			equipe= une_equipe
		)

		# test des propiétés hérités de BaseInscription
		self.assertIsInstance(inscription, BaseInscription)
		self.assertEqual(inscription.identifiant_identite, 0)
		self.assertEqual(inscription.identifiant_equipe, 1)
		self.assertEqual(inscription.date_inscription, debut)
		self.assertEqual(inscription.date_fin, fin)
		self.assertEqual(inscription.date_postule, postule)
		self.assertTrue(inscription.type_inscription, TypeInscription.membre )

		# test des propiétés non hérités de Inscription
		self.assertIsInstance(inscription, Inscription)
		self.assertEqual(inscription.membre, une_identite)
		self.assertEqual(inscription.equipe, une_equipe)
