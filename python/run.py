#! /bin/python3
#-*- coding: utf-8 -*-

import unittest
from src import app
from src.environnement import arguments

# lance le controlleur pour ajoute les routes a l'application
import src.controlleur

doc="""
Code lançant l'application.
"""

if __name__ == '__main__':
	if arguments.debug :
		print('-------- Unittest --------')
		# recherche les testes unitaires dans src/test et les executes avec le repertoir src comme base. 
		all_tests = unittest.defaultTestLoader.discover('./src/test', top_level_dir='./src')
		unittest.TextTestRunner(verbosity=arguments.debug_detail).run(all_tests)
	
	print("-------- Lancement de l'application --------")
	app.run(host=arguments.host, port=arguments.port, debug=arguments.debug)
	
