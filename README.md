# Module équipe

## Description

C'est le code du module equipe




## liens utiles

- https://docs.python.org/fr/3/library/enum.html
- https://docs.python.org/3/library/dataclasses.html
- https://flask-fr.readthedocs.io/  
  Doc de flask
- https://css-tricks.com/snippets/css/a-guide-to-flexbox/  
  Guide pour utiliser le mode flex en css
- documentation pour javascript :
	- https://fr.javascript.info/
	- https://developer.mozilla.org/fr/docs/Web/JavaScript
- https://doc.traefik.io/traefik/  
  Doc de Traefik



## Arborescence des répertoires

- conception :  
  les documents de conception du module.
- mes_secrets :  
  Pour les secrets docker
- sql  
  Le code de la base de donnée.
- docker-compose.yml  
  Le docker compose pour déployer l'application sur les serveurs.
- python  
  Le code du serveur python-flask
  - DockerFile  
    Le fichier permettant la construction du docker python contenant l'application.
  - requirements.txt  
    Les modules pythons utilisés à installer dans le docker python
  - run.py  
    Le fichier de lancement de l'application python. (lance le serveur flask)
  - src  
    Le répertoire contenant les sources en python de l'application.
    - models  
      Le répertoires où se trouvent les classes des modèles.
    - static  
      Le répertoire où se trouvent les fichiers statiques du site ( javascripts / css ... )
      - css  
        Le répertoire où se trouvent les fichiers css du site
      - js  
        Le répertoire où se trouvent les fichiers javascript du site.
    - templates  
      Le répertoire où se trouvent les templates html utilisés pour la construction des pages web.
      - elements  
        répertoire contenant les éléments html inclus dans les différents templates. (ex : menu, head ...)
    - test  
      Le répertoire où se trouvent les tests unitaires de l'application.
    - controlleur.py  
      Le controlleur de l'application. Qui répertorie les chemins des requêtes HTTP et effectuent les traitements. (choix des modèles et templates)
    - environnement.py  
      Le code permettant la gestion des variables d'environnement de l'application (et des secrets docker)
    
